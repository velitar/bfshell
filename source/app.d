import std.stdio;
import std.array : split;
import std.file;
import std.conv : to;
import std.range : drop;

/*  ============================================
    #                                          #
    #                                          #
    #                VARIABLES                 #
    #                                          #
    #                                          #
    ============================================
*/

private char[] dbgChars = new char[0];

void main() {
    Brainfuck bf = new Brainfuck();
    
	writeln("BFShell 0.1 (alfa) (c) Jakub Bucko\nType /help for help.");
    foreach(char[] cmd; lines(stdin)) {
        cmd = cmd[0 .. $-1];
        
        if (cmd[0] != '/') {
            bf.eval(cmd);
        } else {
            char[][] cmdLine = cmd.split(" ");
            switch(cmdLine[0]) {
                default:
                    writefln("Command '%s' not found! Type '/help' for help.", cmdLine[0]);
                    break;
                case "/exit":
                    writeln("Exiting.");
                    return;
                case "/reset":
                    bf = new Brainfuck();
                    break;
                case "/dump":
                    bf.dump();
                    break;
                case "/open":
                    if (cmdLine.length == 1) {
                        writeln("File path required!");
                    } else {
                       char[] code = readTxt(cmdLine[1]);
                       if (code.length > 0) {
                           bf.eval(code);
                       }
                    } 
                    break;
                case "/log":
                    if (cmdLine.length == 1) {
                        writeln("File path required!");
                    } else {
                        bf.setLog(cmdLine[1]);
                    }
                    break;
                case "/dbg":
                    if (cmdLine.length == 1) {
                        writeln("File path required!");
                    } else {
                        dbgChars = readTxt(cmdLine[1]);
                    }
                    break;
                case "/next":
                    if (dbgChars.length == 0) {
                        writeln("No file has been opened for debugging!");
                    } else {
                        bf.eval([dbgChars[0]]);
                        dbgChars.drop(1);
                        if (dbgChars.length == 0) {
                            writeln("End of program");
                        }
                    }
                    break;
                case "/replace":
                    if (cmdLine.length < 3) {
                        writeln("<pos> and <value> required!");
                    } else {
                        bf.replace(to!int("" ~ cmdLine[1]), to!byte(cmdLine[2]));
                    }
                    break;
                case "/help":
                    writeln("Available commands:");
                    writeln(`    /exit exit bfshell
    /reset reset to default state
    /dump print pointer value and contetent of memory
    /open <file> open and evaluate file with BF code
    /log <file> log memory and pointer value to file.
    /dbg <file> debug code in file
    /next eval next command when debugging
    /replace <pos> <value> replace value of cell on <pos>`);
                    break;
                
            }
        }
	}
}

/*  ============================================
    #                                          #
    #                                          #
    #                FUNCTIONS                 #
    #                                          #
    #                                          #
    ============================================
*/
char[] readTxt(in char[] fileName) {
    try {
        return cast(char[])readText(fileName);
    }
    catch (FileException e) {
        stderr.writeln(e.msg);
    }
    return new char[0];
}

/*  ============================================
    #                                          #
    #                                          #
    #                 CLASSES                  #
    #                                          #
    #                                          #
    ============================================
*/

class Brainfuck {
    private byte[] data;
    private ulong pointer = 0;
    private char[] logFile;
    
    this() {
        data.length = 1;
        logFile.length = 0;
    }
    
    void setLog(char[] file) {
        logFile = file;
    }

    void replace(in int pos, in byte value) {
        data[pos] = value;
    }
    
    private char[] formatMemory() {
        char[] str;
        str ~= '[';
        foreach(i, b; data) {
            char c = cast(char) b;
            if (c == '\n') {
                str ~= '\\';
                str ~= 'n';
            } else if (c == '\t') {
                str ~= '\\';
                str ~= 't';
            } else {
                str ~= c;
            }
            
            if (i != data.length - 1) {
                str ~= ',';
                str ~= ' ';
            }
        }
        str ~= ']';
        
        return str;
    }
    
    void dump() {
        writefln("Pointer: %d\n", pointer);
    
        writeln("As bytes");
        writeln(data);
        writeln("\nAs chars");
        writeln(formatMemory());
    }
    
    private void log() {
            char[] logStr = "Pointer: " ~ to!string(pointer) ~ "\n\n" ~ cast(char[])to!string(data) ~ '\n';
            foreach(i; 0..pointer + 1) {
                logStr ~= ' ';
            }
            logStr ~= "^\n" ~ formatMemory();
            foreach(i; 0..pointer + 1) {
                logStr ~= ' ';
            }
            logStr ~= "^\n";
            
            foreach (i; 0..logStr.length) {
                logStr ~= '-';
            }
            
            logStr ~= '\n';
            
            append(logFile, logStr);
    }
    
    void eval(char[] code) {
        int l = 0;
        for(int i = 0; i < code.length; i++) {
            bool logOk = true;
            if (logFile.length == 0) {
                logOk = false;
            }
            
            char c = code[i];
            switch(c) {
                default:
                    logOk = false;
                    break;
                case '>':
                    pointer++;
                    if (pointer == data.length) {
                        data.length++;
                    }
                    
                    break;
                case '<':
                    if (pointer == 0) {
                        pointer = data.length - 1;
                    } else {
                        pointer--;
                    }
                    
                    break;
                case '+':
                    data[pointer]++;
                    
                    break;                
                case '-':
                    data[pointer]--;
                    
                    break;
                case '.':
                    write(cast(char)data[pointer]);
                    
                    break;
                case ',':
                    write("Insert: ");
                    string str = readln();
                    data[pointer] = cast(ubyte)str[0];
                   /* char tmp;
                    readf("%c", tmp);
                    data[pointer] = cast(byte) tmp;*/
                    
                    break;
                case '[':
                    if (data[pointer] == 0) {
                        i++;
                        while(l > 0 || code[i] != ']') {
                            if(code[i] == '[') l++;
                            if(code[i] == ']') l--;
                            i++;
                        }
                    }
                    
                    break;
                case ']':
                    if (data[pointer] != 0) {
                        i--;
                        while(l > 0 || code[i] != '[') {
                            if(code[i] == ']') l++;
                            if(code[i] == '[') l--;
                            i--;
                        }   
                        i--;
                    }
                    
                    break;
            }
            if (logOk) 
                log();
        }
    }
}
